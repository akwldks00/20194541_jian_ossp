Use Apache 2.0 open source license for the original code.
This program modified and used some source codes 
for Ben Zaretzky's [AA SMBZ] Final Model Evaluation Program, 
a participant in Challenges in Presentation Learning: Facial Expression Recreation Challenge.

face_detection model: 
Copyright [2022] [OpenCv]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

Notice about the open source:
Copyright (c) [2018] [Omar Ayman]